/**
 * 生产环境
 */
;
(function() {
    window.SITE_CONFIG = {};

    // api接口请求地址
    window.SITE_CONFIG['baseUrl'] = 'http://localhost:8081/kjb-admin/';
    //单点登录集成平台域名修改
    window.SITE_CONFIG['ssoUrl'] = 'http://xg.xl.kj/#/sso-login?clientId=kjb';

    // cdn地址 = 域名 + 版本号
    window.SITE_CONFIG['domain'] = './'; // 域名
    window.SITE_CONFIG['version'] = ''; // 版本号(年月日时分)
    window.SITE_CONFIG['cdnUrl'] = window.SITE_CONFIG.domain + window.SITE_CONFIG.version;
})();

function retBaseUrl() {
    return window.SITE_CONFIG['baseUrl'];
}