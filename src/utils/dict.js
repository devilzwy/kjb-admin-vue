
const dict = {
  // 密级
  securityLevel: [
    {label: '秘密', value: '1'},
    {label: '机密', value: '2'},
    {label: '绝密', value: '3'},
    {label: '绝密·核心', value: '4'}
  ],
  // 语种
  languageTpye: [
    {label: '中文', value: 'zh'},
    {label: '英文', value: 'en'}
  ],
  // 紧急程度
  warnTpye: [
    {label: '特急', value: 'tj'},
    {label: '紧急', value: 'jj'},
    {label: '常规', value: 'cg'}
  ]

}

export default dict
