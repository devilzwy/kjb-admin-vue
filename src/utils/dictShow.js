import http from '@/utils/httpRequest'

const dictShow = async function (groupName) {
  let obj;
  await http({
    url: http.adornUrl(`/sys/config/dictSelect/`+groupName),
    method: "get",
    params: http.adornParams(),
  }).then(({ data }) => {
    if (data && data.code === 0) {
      obj = data.dict;
    }
  });
  return obj;
}
export default dictShow
