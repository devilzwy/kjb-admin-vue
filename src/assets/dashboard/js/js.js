﻿//定义url前缀
var reqUrl = 'http://localhost:8081/kjb-admin/';

$(window).load(function() {
    $('.loading').fadeOut()
})

/****/
$(document).ready(function() {
    var whei = $(window).width()
    $('html').css({ fontSize: whei / 20 })
    $(window).resize(function() {
        var whei = $(window).width()
        $('html').css({ fontSize: whei / 20 })
    })
})

$(window).load(function() {
    $('.loading').fadeOut()
})
$(function() {
    echart1() //场地使用情况分析
    xlcjzs() //训练成绩展示
    dyxhzs() //弹药消耗
    sbqkfx() //伤病情况分析
    zbztzs() //装备状态展示
    Kjkt() //空降空投
    xljd() //训练进度
    echarts_4()
    bt01()
    bt02()
    bt03()
    bt04()
})

//空降空投
function Kjkt() {
    $.ajax({
        dataType: "json",
        url: reqUrl + "kjb/tjfx/kjktStatistical",
        success: function(data) {
            var table = document.getElementById("tbodyByKjkt");
            var cxkqData = data.data;
            var rs = "";
            for (var i = 0; i < cxkqData.length; i++) {
                rs += "<tr>";
                rs += "<td class='kjtd'>";
                rs += cxkqData[i].xm;
                rs += "</td>";
                rs += "<td class='kjtd'>";
                rs += cxkqData[i].cs;
                rs += "</td>";
                rs += "<td class='kjtd'>";
                rs += cxkqData[i].sj;
                rs += "</td>";
                rs += "<td class='kjtd'>";
                rs += cxkqData[i].sx;
                rs += "</td>";
                rs += "<td class='kjtd'>";
                rs += cxkqData[i].jx;
                rs += "</td>";
                rs += "<td class='kjtd'>";
                rs += cxkqData[i].rwlx;
                rs += "</td>";
                rs += "<td class='kjtd'>";
                rs += cxkqData[i].ky;
                rs += "</td>";
                rs += "</tr>";
            }
            table.innerHTML = rs;
        }
    });
}
//装备状态展示
function zbztzs() {
    //tbodyByZbzs
    $.ajax({
        dataType: "json",
        url: reqUrl + "kjb/tjfx/zbztStatistical",
        success: function(data) {
            var table = document.getElementById("tbodyByZbzs");
            var cxkqData = data.data;
            var rs = "";
            for (var i = 0; i < cxkqData.length; i++) {
                rs += "<tr>";
                rs += "<td>";
                rs += cxkqData[i].lb;
                rs += "</td>";
                rs += "<td class='zbtd'>";
                rs += cxkqData[i].bzs;
                rs += "</td>";
                rs += "<td class='zbtd'>";
                rs += cxkqData[i].sys;
                rs += "</td>";
                rs += "<td class='zbtd'>";
                rs += cxkqData[i].kys;
                rs += "</td>";
                rs += "<td class='zbtd'>";
                rs += cxkqData[i].bfs;
                rs += "</td>";
                rs += "<td class='zbtd'>";
                rs += cxkqData[i].wxs;
                rs += "</td>";
                rs += "</tr>";
            }
            table.innerHTML = rs;
        }
    });
}

//训练成绩展示
function xlcjzs() {
    $.ajax({
        type: 'POST',
        url: reqUrl + 'kjb/tjfx/xlcjStatistical',
        dataType: 'json',
        success: function(data) {
            $('#xlcjzs').empty()
            var listHtml = ''
            data.data.forEach((item) => {
                listHtml = listHtml + '<li><p><span>' + item.mc + '</span><span>' + item.jhmc + '</span><span>' + item.cj + '</span><span>' + (item.pd == '1' ? '单位' : '个人') + '</span></p></li>'
            })

            $('#xlcjzs').append(listHtml)
            $('.wrap,.adduser').liMarquee({
                direction: 'up', //身上滚动
                runshort: false, //内容不足时不滚动
                scrollamount: 20 //速度
            });
            // document.getElementById("xlcjzs").innerHTML=listHtml
        }
    })
}

//伤病情况分析
function sbqkfx() {
    var myChart = echarts.init(document.getElementById('sbqkfx'))
    $.ajax({
        type: 'POST',
        url: reqUrl + 'kjb/tjfx/sbqkStatistical',
        dataType: 'json',
        success: function(data) {
            if (data.code == 0) {
                var colors = ['#0278e6', '#34d160', '#fcdf39', '#f19611', '#00c6ff', '#f76363'].reverse()
                var option = {
                    color: colors,
                    tooltip: {
                        trigger: 'item',
                        formatter: '{c}'
                    },
                    series: [{
                        name: '半径模式',
                        type: 'pie',
                        radius: ['30%', '70%'],
                        center: ['50%', '50%'],
                        roseType: 'radius',
                        minShowLabelAngle: 60,
                        label: {
                            show: true,
                            normal: {
                                position: 'outside',
                                fontSize: 16
                            }
                        },
                        labelLine: {
                            length: 1,
                            length2: 20,
                            smooth: true
                        },
                        data: data.data.maps
                    }]
                }
                myChart.setOption(option)
            }
        }
    })

    window.addEventListener('resize', () => {
        myChart.resize()
    })
}

//训练进度
function xljd() {
    // 基于准备好的dom，初始化echarts实例
    var xljdechart = echarts.init(document.getElementById('xljdechart'))
    $.ajax({
        dataType: "json",
        url: reqUrl + "kjb/tjfx/xljdStatistical",
        success: function(data) {
            console.log(data);
            // 使用刚指定的配置项和数据显示图表。
            var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                legend: {
                    data: ['周', "天"],
                    top: '2%',
                    textStyle: {
                        color: 'rgba(255,255,255,.5)',
                        fontSize: '12',
                    },
                    itemWidth: 12,
                    itemHeight: 12,
                    itemGap: 35
                },
                grid: {
                    left: '0%',
                    top: '40px',
                    right: '0%',
                    bottom: '0%',
                    containLabel: true
                },
                xAxis: [{
                    type: 'category',
                    data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: 'rgba(255,255,255,.1)',
                            width: 1,
                            type: 'solid'
                        },
                    },

                    axisTick: {
                        show: false,
                    },
                    axisLabel: {
                        interval: 0,
                        // rotate:50,
                        show: true,
                        splitNumber: 15,
                        textStyle: {
                            color: 'rgba(255,255,255,.6)',
                            fontSize: '14',
                        },
                    },
                }],
                yAxis: [{
                    type: 'value',
                    minInterval: 1,
                    axisLabel: {
                        //formatter: '{value} %'
                        show: true,
                        textStyle: {
                            color: 'rgba(255,255,255,.6)',
                            fontSize: '14',
                        },
                    },
                    axisTick: {
                        show: false,
                    },
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: 'rgba(255,255,255,.1	)',
                            width: 1,
                            type: 'solid'
                        },
                    },
                    splitLine: {
                        lineStyle: {
                            color: 'rgba(255,255,255,.1)',
                        }
                    }
                }],
                series: [{
                    name: '周',
                    type: 'bar',
                    smooth: true,
                    data: data.data.week,
                    // barWidth: '20%', //柱子宽度
                    // barGap: 1, //柱子之间间距
                    itemStyle: {
                        normal: {
                            color: '#2f89cf',
                            opacity: 1,
                            barBorderRadius: 5,
                        }
                    }
                }, {
                    name: '天',
                    type: 'bar',
                    data: data.data.day,
                    barWidth: '15%', //柱子宽度
                    // barGap: 1, //柱子之间间距
                    itemStyle: {
                        normal: {
                            color: '#62c98d',
                            opacity: 1,
                            barBorderRadius: 5,
                        }
                    }
                }]
            }
            xljdechart.setOption(option)
        }
    })

    window.addEventListener('resize', function() {
        xljdechart.resize()
    })
}

//弹药消耗
function dyxhzs() {
    // 基于准备好的dom，初始化echarts实例
    var myChart2 = echarts.init(document.getElementById('echart3'))
    $.ajax({
        type: 'POST',
        url: reqUrl + 'kjb/tjfx/dyxhStatistical',
        dataType: 'json',
        success: function(data) {
            // 使用刚指定的配置项和数据显示图表。
            var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                legend: {
                    data: ['本年度'],
                    top: '2%',
                    textStyle: {
                        color: 'rgba(255,255,255,.5)',
                        fontSize: '12',
                    },
                    itemWidth: 12,
                    itemHeight: 12,
                    itemGap: 35
                },
                grid: {
                    left: '0%',
                    top: '40px',
                    right: '0%',
                    bottom: '0%',
                    containLabel: true
                },
                xAxis: [{
                    type: 'category',
                    data: data.data.param_key,
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: 'rgba(255,255,255,.1)',
                            width: 1,
                            type: 'solid'
                        },
                    },

                    axisTick: {
                        show: false,
                    },
                    axisLabel: {
                        interval: 0,
                        // rotate:50,
                        show: true,
                        splitNumber: 15,
                        textStyle: {
                            color: 'rgba(255,255,255,.6)',
                            fontSize: '14',
                        },
                    },
                }],
                yAxis: [{
                    type: 'value',
                    minInterval: 1,
                    axisLabel: {
                        //formatter: '{value} %'
                        show: true,
                        textStyle: {
                            color: 'rgba(255,255,255,.6)',
                            fontSize: '14',
                        },
                    },
                    axisTick: {
                        show: false,
                    },
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: 'rgba(255,255,255,.1	)',
                            width: 1,
                            type: 'solid'
                        },
                    },
                    splitLine: {
                        lineStyle: {
                            color: 'rgba(255,255,255,.1)',
                        }
                    }
                }],
                series: [{
                    name: '本年度',
                    type: 'bar',
                    data: data.data.sum,
                    barWidth: '20%', //柱子宽度
                    // barGap: 1, //柱子之间间距
                    itemStyle: {
                        normal: {
                            color: '#2f89cf',
                            opacity: 1,
                            barBorderRadius: 5,
                        }
                    }
                }]
            }
            myChart2.setOption(option)
        }
    })

    window.addEventListener('resize', function() {
        myChart2.resize()
    })
}
//场地使用情况分析
function echart1() {
    var myChart = echarts.init(document.getElementById('echart1'))
    $.ajax({
        type: 'POST',
        url: reqUrl + 'kjb/tjfx/cdsyStatistical',
        dataType: 'json',
        success: function(data) {
            if (data.code == 0) {
                var option = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
                    grid: {
                        top: '15%',
                        right: '3%',
                        left: '10%',
                        bottom: '12%'
                    },
                    xAxis: [{
                        type: 'category',
                        data: data.data.paramkey,
                        axisLine: {
                            lineStyle: {
                                color: 'rgba(255,255,255,0.12)'
                            }
                        },
                        axisLabel: {
                            margin: 10,
                            color: '#e2e9ff',
                            textStyle: {
                                fontSize: 14
                            },
                        },
                    }],
                    yAxis: [{
                        minInterval: 1,
                        axisLabel: {
                            formatter: '{value}',
                            color: '#e2e9ff',
                        },
                        axisLine: {
                            show: false
                        },
                        splitLine: {
                            lineStyle: {
                                color: 'rgba(255,255,255,0.12)'
                            }
                        }
                    }],
                    series: [{
                        type: 'bar',
                        data: data.data.address,
                        barWidth: '20px',
                        itemStyle: {
                            normal: {
                                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                    offset: 0,
                                    color: 'rgba(0,244,255,1)' // 0% 处的颜色
                                }, {
                                    offset: 1,
                                    color: 'rgba(0,77,167,1)' // 100% 处的颜色
                                }], false),
                                barBorderRadius: [30, 30, 30, 30],
                                shadowColor: 'rgba(0,160,221,1)',
                                shadowBlur: 4,
                            }
                        },
                        label: {
                            normal: {
                                // show: true,
                                lineHeight: 30,
                                width: 80,
                                height: 30,
                                backgroundColor: 'rgba(0,160,221,0.1)',
                                borderRadius: 200,
                                position: ['-8', '-60'],
                                distance: 1,
                                formatter: [
                                    '    {d|●}',
                                    ' {a|{c}}     \n',
                                    '    {b|}'
                                ].join(','),
                                rich: {
                                    d: {
                                        color: '#3CDDCF',
                                    },
                                    a: {
                                        color: '#fff',
                                        align: 'center',
                                    },
                                    b: {
                                        width: 1,
                                        height: 30,
                                        borderWidth: 1,
                                        borderColor: '#234e6c',
                                        align: 'left'
                                    },
                                }
                            }
                        }
                    }]
                }
                myChart.setOption(option)
            }
        }
    })
    window.addEventListener('resize', () => {
        myChart.resize()
    })
}

function echarts_4() {
    var myChart = echarts.init(document.getElementById('echart4'))
    option1 = {
        //  backgroundColor: '#00265f',
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: ['test', '字段名称2'],
            top: '5%',
            textStyle: {
                color: '#fff',
                fontSize: '12',

            },

            //itemGap: 35
        },
        grid: {
            left: '0%',
            top: '40px',
            right: '0%',
            bottom: '0',
            containLabel: true
        },
        xAxis: [{
            type: 'category',
            data: ['1', '2', '3', '4', '5', '6', '7'],
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(255,255,255,.1)',
                    width: 1,
                    type: 'solid'
                },
            },
            axisTick: {
                show: false,
            },
            axisLabel: {
                interval: 0,
                // rotate:50,
                show: true,
                //  splitNumber: 2,
                textStyle: {
                    color: 'rgba(255,255,255,.6)',
                    fontSize: '12',
                },
            },
        }],
        yAxis: [{
            type: 'value',
            axisLabel: {
                //formatter: '{value} %'
                show: true,
                textStyle: {
                    color: 'rgba(255,255,255,.6)',
                    fontSize: '12',
                },
            },
            axisTick: {
                show: false,
            },
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(255,255,255,.1	)',
                    width: 1,
                    type: 'solid'
                },
            },
            splitLine: {
                lineStyle: {
                    color: 'rgba(255,255,255,.1)',
                }
            }
        }],
        series: [

            {
                name: 'test',
                type: 'line',
                smooth: true,
                data: [11, 2, 6, 4, 5, 12, 20],
                barWidth: '15',
                // barGap: 1,
                itemStyle: {
                    normal: {
                        color: '#62c98d',
                        opacity: 1,
                        barBorderRadius: 5,
                    }
                }
            },
            {
                name: '字段名称2',
                type: 'line',
                smooth: true,
                data: [22, 11, 8, 13, 10, 13, 10],

                itemStyle: {
                    normal: {
                        color: '#ffc000',
                        opacity: 1,

                        barBorderRadius: 5,
                    }
                }
            }
        ]
    }

    myChart.setOption(option1)
    window.addEventListener('resize', function() {
        myChart.resize()
    })
    $('.sebtn a').click(function() {
        $(this).addClass('active').siblings().removeClass('active')
    })
    $('.sebtn a').eq(0).click(function() {
        myChart.setOption(option1)
    })
    $('.sebtn a').eq(1).click(function() {
        myChart.setOption(option2)
    })
    $('.sebtn a').eq(2).click(function() {
        myChart.setOption(option3)
    })

}

function bt01() {
    var myChart = echarts.init(document.getElementById('bt01'))
    var data1 = 104 //己完成
    var data2 = 18 //未完成
    var data3 = data1 / (data1 + data2) * 100
    option = {
        title: [{
            text: data3.toFixed(1) + '%',
            x: 'center',
            y: '54%',
            textStyle: {
                fontSize: 18,
                fontWeight: 'bold',
                fontStyle: 'normal',
                color: '#fff'
            }
        }, {
            text: '己完成',
            x: 'center',
            y: '68%',
            textStyle: {
                fontSize: 10,
                fontWeight: 'normal',
                fontStyle: 'normal',
                color: 'rgba(255,255,255,.6)'
            }

        }, {
            text: '字段名称4',
            x: 'center',
            y: '20',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bold',
                color: '#fff'
            }

        }],
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b}: {c} ({d}%)'
        },
        color: ['#58c485', '#ea7231'],
        series: [{
            name: '检点',
            type: 'pie',
            center: ['50%', '65%'],
            radius: ['45%', '60%'],
            startAngle: 360,
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: false,
                    textStyle: {
                        fontSize: '30',
                        fontWeight: 'bold'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data: [{
                    value: data1,
                    name: '己完成'
                },
                {
                    value: data2,
                    name: '未完成'

                },

            ]

        }]

    }
    myChart.setOption(option)
    window.addEventListener('resize', function() {
        myChart.resize()
    })
}

function bt02() {
    var myChart = echarts.init(document.getElementById('bt02'))
    var data1 = 14 //己完成
    var data2 = 18 //未完成
    var data3 = data1 / (data1 + data2) * 100
    option = {
        title: [{
            text: data3.toFixed(1) + '%',
            x: 'center',
            y: '54%',
            textStyle: {
                fontSize: 18,
                fontWeight: 'bold',
                fontStyle: 'normal',
                color: '#fff'
            }
        }, {
            text: '己完成',
            x: 'center',
            y: '68%',
            textStyle: {
                fontSize: 10,
                fontWeight: 'normal',
                fontStyle: 'normal',
                color: 'rgba(255,255,255,.6)'
            }

        }, {
            text: '字段名称1',
            x: 'center',
            y: '20',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bold',
                color: '#fff'
            }

        }],
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b}: {c} ({d}%)'
        },
        color: ['#58c485', '#ea7231'],
        series: [{
            name: '检点',
            type: 'pie',
            center: ['50%', '65%'],
            radius: ['45%', '60%'],
            startAngle: 360,
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: false,
                    textStyle: {
                        fontSize: '30',
                        fontWeight: 'bold'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data: [{
                    value: data1,
                    name: '己完成'
                },
                {
                    value: data2,
                    name: '未完成'

                },

            ]

        }]

    }
    myChart.setOption(option)
    window.addEventListener('resize', function() {
        myChart.resize()
    })
}

function bt03() {
    var myChart = echarts.init(document.getElementById('bt03'))
    var data1 = 104 //己完成
    var data2 = 108 //未完成
    var data3 = data1 / (data1 + data2) * 100
    option = {
        title: [{
                text: data3.toFixed(1) + '%',
                x: 'center',
                y: '54%',
                textStyle: {
                    fontSize: 18,
                    fontWeight: 'bold',
                    fontStyle: 'normal',
                    color: '#fff'
                }
            },
            {
                text: '己完成',
                x: 'center',
                y: '68%',
                textStyle: {
                    fontSize: 10,
                    fontWeight: 'normal',
                    fontStyle: 'normal',
                    color: 'rgba(255,255,255,.6)'
                }

            }, {
                text: '字段名称2',
                x: 'center',
                y: '20',
                textStyle: {
                    fontSize: 14,
                    fontWeight: 'bold',
                    color: '#fff'
                }

            }
        ],
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b}: {c} ({d}%)'
        },
        color: ['#58c485', '#ea7231'],
        series: [{
            name: '检点',
            type: 'pie',
            center: ['50%', '65%'],
            radius: ['45%', '60%'],
            startAngle: 360,
            avoidLabelOverlap: false,
            label: {
                show: false,

            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data: [{
                value: data1,
                name: '己完成'

            }, {
                value: data2,
                name: '未完成'
            }, ]

        }]

    }
    myChart.setOption(option)
    window.addEventListener('resize', function() {
        myChart.resize()
    })
}

function bt04() {
    var myChart = echarts.init(document.getElementById('bt04'))
    var data1 = 1000 //己完成
    var data2 = 552 //未完成
    var data3 = data1 / (data1 + data2) * 100
    option = {
        title: [{
            text: data3.toFixed(1) + '%',
            x: 'center',
            y: '54%',
            textStyle: {
                fontSize: 18,
                fontWeight: 'bold',
                fontStyle: 'normal',
                color: '#fff'
            }
        }, {
            text: '己完成',
            x: 'center',
            y: '68%',
            textStyle: {
                fontSize: 10,
                fontWeight: 'normal',
                fontStyle: 'normal',
                color: 'rgba(255,255,255,.6)'
            }

        }, {
            text: '字段名称3',
            x: 'center',
            y: '20',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bold',
                color: '#fff'
            }

        }],
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b}: {c} ({d}%)'
        },
        color: ['#58c485', '#ea7231'],
        series: [{
            name: '检点',
            type: 'pie',
            center: ['50%', '65%'],
            radius: ['45%', '60%'],
            startAngle: 360,
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: false,
                    textStyle: {
                        fontSize: '30',
                        fontWeight: 'bold'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data: [{
                    value: data1,
                    name: '己完成'
                },
                {
                    value: data2,
                    name: '未完成'

                },

            ]

        }]

    }
    myChart.setOption(option)
    window.addEventListener('resize', function() {
        myChart.resize()
    })
}