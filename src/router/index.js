/**
 * 全站路由配置
 *
 * 建议:
 * 1. 代码中路由统一使用name属性跳转(不使用path属性)
 */
import Vue from 'vue'
import Router from 'vue-router'
import http from '@/utils/httpRequest'
import { isURL } from '@/utils/validate'
import { clearLoginInfo } from '@/utils'

Vue.use(Router)

// 开发环境不使用懒加载, 因为懒加载页面太多的话会造成webpack热更新太慢, 所以只有生产环境使用懒加载
const _import = require('./import-' + process.env.NODE_ENV)

// 全局路由(无需嵌套上左右整体布局)
const globalRoutes = [
    { path: '/404', component: _import('common/404'), name: '404', meta: { title: '404未找到' } },
    { path: '/login', component: _import('common/login'), name: 'login', meta: { title: '登录' } },
    { path: '/bigdata', component: _import('common/bigdata'), name: 'bigdata', meta: { title: '大屏' } },
    { path: '/zjy', component: _import('common/zjy'), name: 'zjy', meta: { title: '中间页' } },
    { path: '/tc', component: _import('common/tc'), name: 'tc', meta: { title: '退出' } },
    { path: '/big-data', component: _import('modules/kjb/big-data'), name: 'big-data', meta: { title: '大屏' } },
    { path: '/big-data-lvj', component: _import('modules/kjb/big-data-lvj'), name: 'big-data-lvj', meta: { title: '大屏旅级' } },
    { path: '/big-data-yj', component: _import('modules/kjb/big-data-yj'), name: 'big-data-yj', meta: { title: '大屏营级' } },
    { path: '/big-data-lj', component: _import('modules/kjb/big-data-lj'), name: 'big-data-lj', meta: { title: '大屏连级' } },
    { path: '/big-data-zzjg', component: _import('modules/kjb/big-data-zzjg'), name: 'big-data-zzjg', meta: { title: '大屏组织机构' } },
    { path: '/big-data-zbxx', component: _import('modules/kjb/big-data-zbxx'), name: 'big-data-zbxx', meta: { title: '大屏装备信息' } },
    { path: '/big-data-ryxx', component: _import('modules/kjb/big-data-ryxx'), name: 'big-data-ryxx', meta: { title: '大屏人员信息' } },
    { path: '/big-data-info', component: _import('modules/kjb/big-data-info'), name: 'big-data-info', meta: { title: '训练分析' } },
    { path: '/big-content', component: _import('modules/kjb/big-content'), name: 'big-content', meta: { title: '重要通知' } },
    { path: '/big-content-detail', component: _import('modules/kjb/big-content-detail'), name: 'big-content-detail', meta: { title: '重要通知详情' } },
    { path: '/new-big-data', component: _import('modules/kjb/new-big-data'), name: 'new-big-data', meta: { title: '新大屏' } },
    { path: '/new-big-data-info', component: _import('modules/kjb/new-big-data-info'), name: 'new-big-data-info', meta: { title: '新训练分析' } },
    { path: '/new-big-content-detail', component: _import('modules/kjb/new-big-content-detail'), name: 'new-big-content-detail', meta: { title: '新重要通知详情' } }
]

// 主入口路由(需嵌套上左右整体布局)
const mainRoutes = {
    path: '/',
    component: _import('main'),
    name: 'main',
    redirect: { name: 'home' },
    meta: { title: '主入口整体布局' },
    children: [
        // 通过meta对象设置路由展示方式
        // 1. isTab: 是否通过tab展示内容, true: 是, false: 否
        // 2. iframeUrl: 是否通过iframe嵌套展示内容, '以http[s]://开头': 是, '': 否
        // 提示: 如需要通过iframe嵌套展示内容, 但不通过tab打开, 请自行创建组件使用iframe处理!
        { path: '/home', component: _import('common/home'), name: 'home', meta: { title: '首页' } },
        { path: '/theme', component: _import('common/theme'), name: 'theme', meta: { title: '主题' } },
        { path: '/demo-echarts', component: _import('demo/echarts'), name: 'demo-echarts', meta: { title: 'demo-echarts', isTab: true } },
        { path: '/demo-ueditor', component: _import('demo/ueditor'), name: 'demo-ueditor', meta: { title: 'demo-ueditor', isTab: true } },
        { path: '/xlnjh', component: _import('modules/kjb/tbspecialyearplan'), name: 'xlnjh', meta: { title: '训练年计划' } },
        { path: '/xlyjh', component: _import('modules/kjb/tbspecialmonthplan'), name: 'xlyjh', meta: { title: '训练月计划' } },
        { path: '/xlzjh', component: _import('modules/kjb/tbspecialweekplan'), name: 'xlzjh', meta: { title: '训练周计划' } },
        { path: '/xljdjh', component: _import('modules/kjb/tbstageplan'), name: 'xljdjh', meta: { title: '阶段训练计划' } },
        { path: '/xlzxjh', component: _import('modules/kjb/tbspecial'), name: 'xlzxjh', meta: { title: '专项训练计划' } },
        { path: '/xlssjh', component: _import('modules/kjb/tbspecialprojectplan'), name: 'xlssjh', meta: { title: '四实计划' } },


    ],
    beforeEnter(to, from, next) {
        let token = Vue.cookie.get('token');
        console.log("从c中获取t为" + token);
        if (!token || !/\S/.test(token)) {
            clearLoginInfo()
            console.log("跳去login了");
            next({ name: 'login' })
        }
        next()
    }
}

const router = new Router({
    mode: 'hash',
    scrollBehavior: () => ({ y: 0 }),
    isAddDynamicMenuRoutes: false, // 是否已经添加动态(菜单)路由
    routes: globalRoutes.concat(mainRoutes)
})

router.beforeEach((to, from, next) => {
    // 添加动态(菜单)路由
    // 1. 已经添加 or 全局路由, 直接访问
    // 2. 获取菜单列表, 添加并保存本地存储
    if (router.options.isAddDynamicMenuRoutes || fnCurrentRouteType(to, globalRoutes) === 'global') {
        next()
    } else {
        http({
            url: http.adornUrl('/sys/menu/nav'),
            method: 'get',
            params: http.adornParams()
        }).then(({ data }) => {
            if (data && data.code === 0) {
                fnAddDynamicMenuRoutes(data.menuList)
                router.options.isAddDynamicMenuRoutes = true
                sessionStorage.setItem('menuList', JSON.stringify(data.menuList || '[]'))
                sessionStorage.setItem('permissions', JSON.stringify(data.permissions || '[]'))
                next({...to, replace: true })
            } else {
                sessionStorage.setItem('menuList', '[]')
                sessionStorage.setItem('permissions', '[]')
                next()
            }
        }).catch((e) => {
            router.push({ name: 'login' })
        })
    }
})

/**
 * 判断当前路由类型, global: 全局路由, main: 主入口路由
 * @param {*} route 当前路由
 */
function fnCurrentRouteType(route, globalRoutes = []) {
    var temp = []
    for (var i = 0; i < globalRoutes.length; i++) {
        if (route.path === globalRoutes[i].path) {
            return 'global'
        } else if (globalRoutes[i].children && globalRoutes[i].children.length >= 1) {
            temp = temp.concat(globalRoutes[i].children)
        }
    }
    return temp.length >= 1 ? fnCurrentRouteType(route, temp) : 'main'
}

/**
 * 添加动态(菜单)路由
 * @param {*} menuList 菜单列表
 * @param {*} routes 递归创建的动态(菜单)路由
 */
function fnAddDynamicMenuRoutes(menuList = [], routes = []) {
    var temp = []
    for (var i = 0; i < menuList.length; i++) {
        if (menuList[i].list && menuList[i].list.length >= 1) {
            temp = temp.concat(menuList[i].list)
        } else if (menuList[i].url && /\S/.test(menuList[i].url)) {
            menuList[i].url = menuList[i].url.replace(/^\//, '')
            var route = {
                    path: menuList[i].url.replace('/', '-'),
                    component: null,
                    name: menuList[i].url.replace('/', '-'),
                    meta: {
                        menuId: menuList[i].menuId,
                        title: menuList[i].name,
                        isDynamic: true,
                        isTab: true,
                        iframeUrl: ''
                    }
                }
                // url以http[s]://开头, 通过iframe展示
            if (isURL(menuList[i].url)) {
                route['path'] = `i-${menuList[i].menuId}`
                route['name'] = `i-${menuList[i].menuId}`
                route['meta']['iframeUrl'] = menuList[i].url
            } else {
                try {
                    route['component'] = _import(`modules/${menuList[i].url}`) || null
                } catch (e) {}
            }
            routes.push(route)
        }
    }
    if (temp.length >= 1) {
        fnAddDynamicMenuRoutes(temp, routes)
    } else {
        mainRoutes.name = 'main-dynamic'
        mainRoutes.children = routes
        router.addRoutes([
            mainRoutes,
            { path: '*', redirect: { name: '404' } }
        ])
        sessionStorage.setItem('dynamicMenuRoutes', JSON.stringify(mainRoutes.children || '[]'))
    }
}

export default router